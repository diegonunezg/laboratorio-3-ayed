/* Clase nodo, contiene a un estudiante junto a punteros derecha e izquierda */

#include "Student.h"

using namespace std;

class TreeNode {
    public:
        TreeNode(Student s) : student(s), left(NULL), right(NULL) {}

    private:
        Student student;
        TreeNode *left, *right;
        friend class BinaryTree;
};
